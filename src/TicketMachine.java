import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicketMachine {
    private JButton gyudonButton;
    private JButton unadonButton;
    private JButton katsudonButton;
    private JButton ebitendonButton;
    private JButton oyakodonButton;
    private JButton kaisendonButton;
    private JButton checkOutButton;
    private JTextArea orderedItemsList;
    private JPanel root;
    private JLabel totalArea;
    private JButton cancelButton;
    private JTextArea priceArea;

    int sum = 0;

    String addMisoSoup(String foodname) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to add Miso Soup with " + foodname + "?",
                "Miso Soup",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + "   + Miso Soup\n");
            String currentPrice = priceArea.getText();
            priceArea.setText(currentPrice + 150 + " yen\n");
            sum += 150;
            totalArea.setText(sum + " yen");
            foodname += " with Miso Soup";
        }
        return foodname;
    }

    void order(String foodname, int price){
        String[] riceOptions = {"Small", "Regular", "Large"};
        String selectedRiceSize = (String) JOptionPane.showInputDialog(
                null,
                "Please select the rice size:",
                "Rice Size",
                JOptionPane.PLAIN_MESSAGE,
                null,
                riceOptions,
                riceOptions[1]
        );
        if (selectedRiceSize != null) {
            switch (selectedRiceSize) {
                case "Small":
                    price -= 100;
                    break;
                case "Regular":
                    break;
                case "Large":
                    price += 100;
                    break;
            }
            foodname += "(" + selectedRiceSize + ")";
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to order " + foodname + "?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if (confirmation == 0) {
                String currentText = orderedItemsList.getText();
                orderedItemsList.setText(currentText + foodname + "\n");
                String currentPrice = priceArea.getText();
                priceArea.setText(currentPrice + price + " yen\n");
                sum += price;
                totalArea.setText(sum + " yen");
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + addMisoSoup(foodname) + "!");
            }
        }
    }

    void clearOrder() {
        orderedItemsList.setText(null);
        priceArea.setText(null);
        sum = 0;
        totalArea.setText(0 + " yen");
    }

    public TicketMachine() {
        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyudon", 500);
            }
        });
        gyudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/gyudon.png")
        ));
        katsudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Katsudon", 600);
            }
        });
        katsudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/katsudon.png")
        ));
        oyakodonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oyakodon", 450);
            }
        });
        oyakodonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/oyakodon.png")
        ));
        unadonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unadon", 700);
            }
        });
        unadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/unadon.png")
        ));
        ebitendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ebitendon", 650);
            }
        });
        ebitendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/ebitendon.png")
        ));
        kaisendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kaisendon", 800);
            }
        });
        kaisendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("./png/kaisendon.png")
        ));
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel your order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Your order was cancelled.");
                    clearOrder();
                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentText = orderedItemsList.getText();
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + " yen.");
                    clearOrder();
                }
            }
        });

    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("TicketMachine");
        frame.setContentPane(new TicketMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
